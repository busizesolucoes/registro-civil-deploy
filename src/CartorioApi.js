const apiKeyHeaderValue = '7CojClx9l62Mz6SJcEHFWZfK2NtSHXgI';
const apiKeyHeaderName = 'apikey';
const baseUrl = 'https://dev-api.registrocivil.org.br:8443/api/cartorios';

const getReqHeaders = () => {
    const reqHeaders = new Headers();
    reqHeaders.append(apiKeyHeaderName, apiKeyHeaderValue);
    return reqHeaders;
};

const CartorioApi = {};

CartorioApi.getUfs = () => {
    return new Promise((resolve, reject) => {
        const reqHeaders = getReqHeaders();

        const reqOpts = {
            method: 'GET',
            headers: reqHeaders,
            mode: 'cors',
            cache: 'default'
        };

        fetch(baseUrl + '/uf', reqOpts)
            .then(function(response) {
                if (response.ok) {
                    return response.json();
                } else {
                    const e = new Error('Error fetching UFs');
                    throw e;
                }
            })
            .then(responseData => resolve(responseData))
            .catch(e => reject(e));
    });
};

CartorioApi.getCidades = (uf = '') => {
    return new Promise((resolve, reject) => {
        const reqHeaders = getReqHeaders();

        const reqOpts = {
            method: 'GET',
            headers: reqHeaders,
            mode: 'cors',
            cache: 'default'
        };

        fetch(baseUrl + '/uf/' + uf + '/cidade', reqOpts)
            .then(function(response) {
                if (response.ok) {
                    return response.json();
                } else {
                    const e = new Error('Error fetching Cidades');
                    throw e;
                }
            })
            .then(responseData => resolve(responseData.cidades))
            .catch(e => reject(e));
    });
};

CartorioApi.getCartorios = (cidade = 0) => {
    return new Promise((resolve, reject) => {
        const reqHeaders = getReqHeaders();

        const reqOpts = {
            method: 'GET',
            headers: reqHeaders,
            mode: 'cors',
            cache: 'default'
        };

        fetch(baseUrl + '/cidade/' + cidade + '/cartorio', reqOpts)
            .then(function(response) {
                if (response.ok) {
                    return response.json();
                } else {
                    const e = new Error('Error fetching Cartorios');
                    throw e;
                }
            })
            .then(responseData => resolve(responseData.cartorios))
            .catch(e => reject(e));
    });
};

export default CartorioApi;